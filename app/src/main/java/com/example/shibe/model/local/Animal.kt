package com.example.shibe.model.local

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.shibe.model.utils.AnimalType

// Room entity includes fields for each column in the corresponding table in the database
// By default, Room uses the class name as the db table name. The field names are column names by default
@Entity
data class Animal(

    // Each entity requires a primaryKey
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val type: AnimalType,
    val image: String
)