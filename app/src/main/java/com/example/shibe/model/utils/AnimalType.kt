package com.example.shibe.model.utils

enum class AnimalType {
    SHIBE, CAT, BIRD
}