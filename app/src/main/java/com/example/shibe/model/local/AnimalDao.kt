package com.example.shibe.model.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.shibe.model.utils.AnimalType

@Dao
// Can define dao as an interface or abstract class. In general, use an interface
interface AnimalDao {
    // Add functions without a body, then Room will do what we need with them
    // Make them suspend functions since this could take a while

    @Query("SELECT * FROM Animal")
    suspend fun getAll(): List<Animal>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAnimal(vararg animal: Animal) // vararg == variable argument. Can be one or a list of animals

    @Query("SELECT * FROM Animal WHERE type = :animalType") // : is used to reference variable we're using in function
    suspend fun getAllTypedAnimals(animalType: AnimalType):List<Animal>
}