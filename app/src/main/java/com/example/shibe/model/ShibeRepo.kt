package com.example.shibe.model

import android.content.Context
import com.example.shibe.model.local.Animal
import com.example.shibe.model.local.AnimalDb
import com.example.shibe.model.remote.ShibeService
import com.example.shibe.model.utils.AnimalType
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

// Repo coordinates between API & DB
class ShibeRepo(context : Context) {

    private val shibeService = ShibeService.getInstance()
    private val shibeDao = AnimalDb.getInstance(context).animalDao()

    suspend fun getShibes(): List<Animal> = withContext(Dispatchers.IO) {
        val cachedShibes = shibeDao.getAllTypedAnimals(AnimalType.SHIBE)
        return@withContext cachedShibes.ifEmpty {
            // Go to network
            val remoteShibes = shibeService.getShibes()

            // Map list of strings to a Shibe entity
            val entities: List<Animal> = remoteShibes.map {
                Animal(type = AnimalType.SHIBE, image = it)
            }

            // Save shibes
            shibeDao.insertAnimal(*entities.toTypedArray())
            // Return a list of all shibes
            return@ifEmpty entities
        }
    }


}