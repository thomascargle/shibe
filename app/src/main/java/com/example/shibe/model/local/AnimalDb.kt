package com.example.shibe.model.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import kotlinx.coroutines.internal.synchronized

// For entities put in which tables you'll have in the db
@Database(entities = [Animal::class], version = 1)
abstract class AnimalDb : RoomDatabase() {

    abstract fun animalDao(): AnimalDao

    companion object {
        private const val DATABASE_NAME = "Animal.db"

        // Volatile because we don't want to cache this
        @Volatile
        // nullable because we won't have the db right off the bat
        private var instance: AnimalDb? = null

        fun getInstance(context: Context):AnimalDb {
            return instance ?: kotlin.synchronized(this) {
                // Do something in case the instance is null
                instance ?: buildDatabase(context).also {
                    instance = it
                }
            }
        }

        private fun buildDatabase(context: Context): AnimalDb {
            return Room
                .databaseBuilder(context, AnimalDb::class.java, DATABASE_NAME)
                .build()
        }

    }

}