package com.example.shibe.model.remote

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Query

interface ShibeService {

    companion object {
        private const val BASE_URL = "https://shibe.online"
        private const val DOGS_ENDPOINT = "/api/shibes"
        private const val CATS_ENDPOINT = "api/cats"

        fun getInstance(): ShibeService = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create()
    }

    @GET(DOGS_ENDPOINT)
    suspend fun getShibes(@Query("count") count: Int = 10): List<String>

}